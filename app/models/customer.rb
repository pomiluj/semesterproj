class Customer < ActiveRecord::Base
  belongs_to :state
  has_many :quotes
  validates_associated :state
  validates :customer_name, presence: true
  validates :state_id, presence: true
end
