class CreateFinances < ActiveRecord::Migration
  def change
    create_table :finances do |t|
      t.decimal :loan
      t.decimal :interest
      t.integer :term

      t.timestamps null: false
    end
  end
end
