class Finance < ActiveRecord::Base
validates :loan, presence: true
validates :interest, presence: true
validates :term, presence: true


  def self.pmt
    @@pmt
  end

  #calculate values in table and return as finance array according to formula
  def self.amort_table(loan = 0.00, interest = 0.00, term = 0)
    Rails.logger.debug "Inputs: #{loan} #{interest} #{term}"
    finance = []
    fixed = loan
    (1..term*12).each do |i|
      rate = interest/12
      @@pmt = ((rate*fixed)/(1-(1+rate)**(-term*12)))
      finance << loan - (@@pmt - (loan*rate))

      last_amount = loan - (@@pmt - (loan*rate))

      loan = last_amount
    end
    return finance
  end

  def amort_table
    return self.class.amort_table(self.loan, self.interest, self.term)
  end
end
