class Staff < ActiveRecord::Base
  has_many :quotes

  validates :staff_name, presence: true
end
