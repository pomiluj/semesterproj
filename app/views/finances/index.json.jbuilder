json.array!(@finances) do |finance|
  json.extract! finance, :id, :loan, :interest, :term
  json.url finance_url(finance, format: :json)
end
