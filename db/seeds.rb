# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Car.create([{model: 'Mazda 3', color: 'Red', VIN: 'JHMZE2H3XBS851537'}])
Car.create([{model: 'Silverado', color: 'Black', VIN: '1zvbp8ffxc5008545' }])
Car.create([{model: 'Infinity G6', color: 'White', VIN: '2GCEC19V7Y1534758'}])
Car.create([{model: 'Acura', color: 'Blue', VIN: '2CNDL23F066922171'}])
Car.create([{model: 'Charger', color: 'Black', VIN: '1FTZR11EX3P038745'}])
Car.create([{model: 'Mustang', color: 'Tan', VIN: '5N1AA08B24N594313'}])
Car.create([{model: 'Porsche 944', color: 'Brown', VIN: '4F4YR16V2WT567133'}])
Car.create([{model: 'Coronet', color: 'Cyan', VIN: '1GKEK16TX1J605960'}])
Car.create([{model: 'Dodge Journey', color: 'Dark Red', VIN: 'JTDBF30K760384362'}])
Car.create([{model: 'Splash', color: 'Indigo', VIN: '1HGCG56571A982521'}])
Car.create([{model: 'Landy', color: 'Navy', VIN: '1D3HW22N55S790176'}])
Car.create([{model: 'Escudo', color: 'Red', VIN: '1FTWX30507E431712'}])
Car.create([{model: 'Volvo PCC', color: 'Purple', VIN: '5TDJW5G12BS980507'}])
Car.create([{model: 'Volvo Phillip', color: 'White', VIN: '1FMYU04161K353223'}])
Car.create([{model: 'VW Golf', color: 'Black', VIN: 'WAUMGAFL2BA200277'}])
Car.create([{model: 'VW Polo', color: 'Teal', VIN: '1D3HW22N55S790176'}])
Car.create([{model: 'Dodge Durango', color: 'Dark Gray', VIN: 'JS3TE0D18B4719060'}])
Car.create([{model: 'Acura Integra', color: 'Olive', VIN: 'JT3GP10V7X0357179'}])
Car.create([{model: 'Acura TSX', color: 'Dark Blue', VIN: '1FAFP52U01G386419'}])
Car.create([{model: 'Honda Jazz', color: 'Dark Red', VIN: '1J8GS48KX7C233293'}])
Car.create([{model: 'Honda Fit', color: 'Pink', VIN: 'JTEBT14RX80413386'}])
Car.create([{model: 'Honda Pilot', color: 'Black', VIN: '3VWRC29M2YM245632'}])
Car.create([{model: 'Honda Stream', color: 'Grey', VIN: '1YHP80D56493230'}])
Car.create([{model: 'Honda Brio', color: 'White', VIN: 'SAJWA01B87H914730'}])


Staff.create([{staff_name: 'Mike Rowe'}])
Staff.create([{staff_name: 'Kaylee Kocurek'}])
Staff.create([{staff_name: 'Steve Bending'}])
Staff.create([{staff_name: 'Ivan Macias'}])
Staff.create([{staff_name: 'Paul Blart'}])

Quote.create([{wholesale: 13000, markup: 14300, interest: 0.15, principle: 3000, total: 14500, status: '0', staff_id: 3, customer_id: 1, car_id: 2}])
Quote.create([{wholesale: 8000, markup: 8800, interest: 0.08, principle: 1000, total: 8560, status: '0', staff_id: 5, customer_id: 2, car_id: 20}])
Quote.create([{wholesale: 26000, markup: 28600, interest: 0.1, principle: 5000, total: 28100, status: '1', staff_id: 4, customer_id: 3, car_id: 7}])
Quote.create([{wholesale: 18000, markup: 19800, interest: 0.17, principle: 6000, total: 20040, status: '0', staff_id: 4, customer_id: 4, car_id: 22}])
Quote.create([{wholesale: 20000, markup: 22000, interest: 0.2, principle: 2000, total: 23600, status: '0', staff_id: 5, customer_id: 5, car_id: 18}])
Quote.create([{wholesale: 40000, markup: 44000, interest: 0.25, principle: 8000, total: 48000, status: '0', staff_id: 5, customer_id: 6, car_id: 13}])
Quote.create([{wholesale: 33000, markup: 36300, interest: 0.0725, principle: 6000, total: 64957.5, status: '1', staff_id: 1, customer_id: 7, car_id: 6}])
Quote.create([{wholesale: 17000, markup: 18700, interest: 0.05, principle: 1700, total: 17765, status: '0', staff_id: 2, customer_id: 8, car_id: 21}])
Quote.create([{wholesale: 35000, markup: 38500, interest: 0.08, principle: 3500, total: 37520, status: '1', staff_id: 4, customer_id: 9, car_id: 19}])
Quote.create([{wholesale: 8000, markup: 8800, interest: 0.11, principle: 500, total: 8825, status: '1', staff_id: 5, customer_id: 10, car_id: 24}])
Quote.create([{wholesale: 13000, markup: 14300, interest: 0.06, principle: 800, total: 13732, status: '1', staff_id: 2, customer_id: 11, car_id: 15}])
Quote.create([{wholesale: 14000, markup: 15400, interest: 0.045, principle: 1200, total: 14576, status: '1', staff_id: 4, customer_id: 12, car_id: 23}])
Quote.create([{wholesale: 15000, markup: 16500, interest: 0.1, principle: 2500, total: 16250, status: '1', staff_id: 5, customer_id: 13, car_id: 16}])
Quote.create([{wholesale: 36000, markup: 39600, interest: 0.08, principle: 3200, total: 38624, status: '0', staff_id: 3, customer_id: 14, car_id: 8}])
Quote.create([{wholesale: 40000, markup: 44000, interest: 0.075, principle: 4200, total: 42685, status: '0', staff_id: 2, customer_id: 15, car_id: 14}])
Quote.create([{wholesale: 11000, markup: 12100, interest: 0.065, principle: 800, total: 11663, status: '1', staff_id: 3, customer_id: 16, car_id: 5}])
Quote.create([{wholesale: 40000, markup: 44000, interest: 0.09, principle: 6000, total: 43060, status: '0', staff_id: 5, customer_id: 17, car_id: 11}])
Quote.create([{wholesale: 13000, markup: 14300, interest: 0.08, principle: 1000, total: 13960, status: '1', staff_id: 1, customer_id: 18, car_id: 2}])
Quote.create([{wholesale: 14000, markup: 15400, interest: 0.075, principle: 1200, total: 14960, status: '1', staff_id: 3, customer_id: 19, car_id: 10}])
Quote.create([{wholesale: 39000, markup: 42900, interest: 0.125, principle: 4500, total: 43312.5, status: '1', staff_id: 5, customer_id: 20, car_id: 4}])


Customer.create([{customer_name: 'Tyler Kocurek', state_id: 3}])
Customer.create([{customer_name: 'Doyle Odom', state_id: 4}])
Customer.create([{customer_name: 'Octavia Odom', state_id: 2}])
Customer.create([{customer_name: 'Irene Guajardo', state_id: 1}])
Customer.create([{customer_name: 'Zane Guajardo', state_id: 1}])
Customer.create([{customer_name: 'Tim Baird', state_id: 4}])
Customer.create([{customer_name: 'Cullen Brown', state_id: 2}])
Customer.create([{customer_name: 'Phillip Phares', state_id: 2}])
Customer.create([{customer_name: 'Bill Malloy', state_id: 2}])
Customer.create([{customer_name: 'James Shadid', state_id: 4}])
Customer.create([{customer_name: 'Dana Kees', state_id: 2}])
Customer.create([{customer_name: 'Logan Gardner', state_id: 3}])
Customer.create([{customer_name: 'Albert Sawalha', state_id: 2}])
Customer.create([{customer_name: 'Christina Sawalha', state_id: 2}])
Customer.create([{customer_name: 'Ian Marmottin', state_id: 2}])
Customer.create([{customer_name: 'Megan Marmottin', state_id: 4}])
Customer.create([{customer_name: 'Dylan Cutrer', state_id: 3}])
Customer.create([{customer_name: 'Angelee Cutrer', state_id: 2}])
Customer.create([{customer_name: 'Travis Finigan', state_id: 3}])
Customer.create([{customer_name: 'Brenna Janka', state_id: 2}])

State.create([{state_name: 'Great'}])
State.create([{state_name: 'Good'}])
State.create([{state_name: 'Neutral'}])
State.create([{state_name: 'Bad'}])
State.create([{state_name: 'Banned'}])

Finance.create([{loan: 15000, interest: 0.10, term: 4}])
Finance.create([{loan: 18000, interest: 0.08, term: 5}])
Finance.create([{loan: 5000, interest: 0.16, term: 3}])
Finance.create([{loan: 20000, interest: 0.05, term: 4}])
Finance.create([{loan: 12000, interest: 0.12, term: 3}])
