json.array!(@quotes) do |quote|
  json.extract! quote, :id, :wholesale, :markup, :interest, :principle, :total, :status, :staff_id, :customer_id, :car_id
  json.url quote_url(quote, format: :json)
end
