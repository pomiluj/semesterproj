class Car < ActiveRecord::Base
  has_one :quote
  validates :model, presence: true
  validates :color, presence: true
  validates :VIN, presence: true

end
