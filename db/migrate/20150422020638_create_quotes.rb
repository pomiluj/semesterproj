class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.decimal :wholesale
      t.decimal :markup
      t.decimal :interest
      t.decimal :principle
      t.decimal :total
      t.string :status
      t.integer :staff_id
      t.integer :customer_id
      t.integer :car_id

      t.timestamps null: false
    end
  end
end
