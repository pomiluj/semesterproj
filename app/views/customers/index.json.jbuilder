json.array!(@customers) do |customer|
  json.extract! customer, :id, :customer_name, :state_id
  json.url customer_url(customer, format: :json)
end
