# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
build_table = ->
  loan =  $("#finance_loan").val()
  interest = $("#finance_interest").val()
  term = $("#finance_term").val()
  $.get "/finances/finance",
    {loan: loan, interest: interest , term: term}
#the '$' is the character indicating jquery
$ ->
  $('#finance_loan, #finance_interest, #finance_term').bind 'keyup mouseup mousewheel', ->
    build_table()

  build_table()