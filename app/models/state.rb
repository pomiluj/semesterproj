class State < ActiveRecord::Base
  has_many :customers
  validates :state_name, presence: true
end
