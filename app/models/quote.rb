class Quote < ActiveRecord::Base
  belongs_to :car
  belongs_to :staff
  belongs_to :customer
  validates_associated :car
  validates_associated :staff
  validates_associated :customer
  validates :wholesale, presence: true

  validates :interest, presence: true
  validates :principle, presence: true

  validates :status, presence: true
  validates :staff_id, presence: true
  validates :customer_id, presence: true
  validates :car_id, presence: true

  def status_value
    if self.status == "0"
      return "Pending"
    else
      return "Sold"
    end
  end

  def markup_calc
    mark = wholesale + (wholesale * 0.10)
    return mark
  end

  def total_calc
    cal = markup_calc + (markup_calc * interest) - principle
    tot = cal + (cal * 0.043)
    return tot
  end

  def markamt
    amt = wholesale * 0.10
    return amt
  end

  def taxsale
    tax = markup_calc * 0.043
    return tax
  end

  def self.profit
    profit = 0
    Quote.all.each do |i|
      if i.status == "1"
        profit += i.total_calc
      end
    end

    return profit
  end

  def self.markupsale
    revenue = 0
    Quote.all.each do |i|
      if i.status == "1"
        revenue += i.markamt
      end
    end
    return revenue
  end

  def self.taxrev
    taxrevenue = 0
    Quote.all.each do |i|
      if i.status == "1"
        taxrevenue += i.taxsale
      end
    end
    return taxrevenue
  end
end
